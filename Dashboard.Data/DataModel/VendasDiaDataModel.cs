﻿using System;
using SQLite.Net.Attributes;

namespace Dashboard.Data
{
	[Table("TBVendasDia")]
	public class VendasDiaDataModel
	{
		#region Properties

		[PrimaryKey]
		public string loja { get; set; }
		public string cidade { get; set; }
		public float total { get; set; }

		#endregion
	}
}

