﻿using System;
using SQLite.Net;
using System.Threading.Tasks;
using Dashboard.Data.Context;

namespace Dashboard.Data
{
	public class SQLiteContext : ISQLiteContext, IDisposable
	{
	
		#region Fields

		public ISQLiteDataPlatform SQLiteDataPlatform{ get; private set; } 

		#endregion

		#region Constructor

		public SQLiteContext (ISQLiteDataPlatform sqliteDataPlaform )
		{
			SQLiteDataPlatform = sqliteDataPlaform;

            Connection = new SQLiteConnectionWithCustomLock(SQLiteDataPlatform.Platform, sqliteDataPlaform.Path);
            
		}

		#endregion

		#region ISQLiteContext implementation

        public SQLiteConnectionWithCustomLock Connection { get; set; }

		#endregion

		#region Methods

        public void CreateTableAsync(params Type[] types) 
        {
            Task.Factory.StartNew(() =>
            {
                using (Connection.Lock())
                {
                    foreach (Type type in types)
                        Connection.CreateTable(type);
                }
            });
        }

		public void Dispose ()
		{
			Connection = null;
		}

		#endregion
		
	}
}



