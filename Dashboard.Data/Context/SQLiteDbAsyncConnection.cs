﻿using System;
using SQLite.Net.Async;
using SQLite.Net;
using System.Threading.Tasks;

namespace Dashboard.Data
{
	public class SQLiteDbAsyncConnection: SQLiteAsyncConnection
	{
		#region Constructor

		public SQLiteDbAsyncConnection(Func<SQLiteConnectionWithLock> connection)
			: base(connection)
		{
		}

		#endregion

		#region Methods

		public async Task CreateDbTableAsync<T>()
			where T : class
		{
			await CreateTableAsync<T>();
		}


		#endregion
	}
}


