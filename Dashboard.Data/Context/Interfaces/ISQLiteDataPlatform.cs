﻿using System;

namespace Dashboard.Data
{
	public interface ISQLiteDataPlatform
	{
		string Path { get;  }
		SQLite.Net.Interop.ISQLitePlatform Platform { get; }
	}
}

