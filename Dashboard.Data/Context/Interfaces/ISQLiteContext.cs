﻿using Dashboard.Data.Context;
using SQLite.Net;
using System;


namespace Dashboard.Data
{
	public interface ISQLiteContext : IDisposable
	{
        SQLiteConnectionWithCustomLock Connection { get; set; }
        void CreateTableAsync(params Type[] types); 
	}
}

