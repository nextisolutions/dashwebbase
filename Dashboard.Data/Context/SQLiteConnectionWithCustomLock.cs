﻿
using SQLite.Net;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;

namespace Dashboard.Data.Context
{
    public class SQLiteConnectionWithCustomLock : SQLiteConnection, IDisposable
    {
        #region Fields

        private object _lock = new object();

        #endregion

        #region Constructor

        public SQLiteConnectionWithCustomLock(ISQLitePlatform sqlitePlatform, string databasePath, bool storeDateTimeAsTicks = true, IBlobSerializer serializer = null, IDictionary<string, TableMapping> tableMappings = null, IDictionary<Type, string> extraTypeMappings = null, IContractResolver resolver = null)
            : base(sqlitePlatform, databasePath, storeDateTimeAsTicks, serializer, tableMappings, extraTypeMappings, resolver)
        { }

        public SQLiteConnectionWithCustomLock(ISQLitePlatform sqlitePlatform, string databasePath, SQLiteOpenFlags openFlags, bool storeDateTimeAsTicks = true, IBlobSerializer serializer = null, IDictionary<string, TableMapping> tableMappings = null, IDictionary<Type, string> extraTypeMappings = null, IContractResolver resolver = null)
            : base(sqlitePlatform, databasePath, openFlags, storeDateTimeAsTicks, serializer, tableMappings, extraTypeMappings, resolver)
        { }

       
        #endregion

        #region Methods

        public IDisposable Lock()
        {
            lock (this) 
            {
                
            }

            return this;
        }


        #endregion

    }


    public class DiposableConnection : IDisposable
    {
        #region Fields

        private SQLiteConnection _conn;

        #endregion

        #region Constructor
        public DiposableConnection(SQLiteConnection connection)
        {
            _conn = connection;
        } 

        #endregion

        #region Methods

        public void Dispose()
        {
            _conn.Dispose();
        }

        #endregion

        
    }
}
