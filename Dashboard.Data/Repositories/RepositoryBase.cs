﻿using System;
using Dashboard.Domain;
using System.Threading.Tasks;
using System.Collections.Generic;
using SQLite.Net.Async;
using SQLite.Net;
using System.Linq;
using AutoMapper;
using Dashboard.Data.Context;

namespace Dashboard.Data
{
    public abstract class RepositoryBase<TEntity, TDataModel> : IRepositoryBase<TEntity>
        where TEntity : class
        where TDataModel : class
    {

        #region Fields

        private ISQLiteContext _context;
        private Lazy<IList<TEntity>> _entities;
        private bool _isDisposed = false;
        private TableQuery<TEntity> _innerQuery;
        private bool _isUpdated;

        #endregion

        #region Constructor

        public RepositoryBase(ISQLiteContext context)
        {
            _context = context;
            _entities = new Lazy<IList<TEntity>>(() => 
            {
                using (var conn = Open())
                {
                    var result = conn.Table<TDataModel>().ToList();
                    return Mapper.Map<List<TDataModel>, List<TEntity>>(result);
                }
            });
            _isUpdated = true;
        }

        #endregion

        #region IRepositoryBase implementation

        public virtual int Count
        {
            get { return _entities.Value.Count; }
        }

        public virtual void Add(TEntity entity)
        {
            using (var conn = Open())
            {
                var mapper = Mapper.Map<TEntity, TDataModel>(entity);
                _context.Connection.Insert(mapper);
                SaveChanges();
            }
            _isUpdated = false;
        }

        public virtual void AddAll(List<TEntity> entities)
        {
            using (var conn = Open())
            {
                var mapper = Mapper.Map<List<TEntity>, List<TDataModel>>(entities);
                _context.Connection.InsertOrReplaceAll(mapper);
                SaveChanges();
            }
            _isUpdated = false;
        }





        public virtual void Update(TEntity entity)
        {
            using (var conn = Open())
            {
                var mapper = Mapper.Map<TEntity, TDataModel>(entity);
                _context.Connection.Update(mapper);
                SaveChanges();
            }
            _isUpdated = false;
        }

        public virtual void Remove(TEntity entity)
        {
            using (var conn = Open())
            {
                var mapper = Mapper.Map<TEntity, TDataModel>(entity);
                _context.Connection.Delete(mapper);
                SaveChanges();
            }
            _isUpdated = false;
        }

        public TEntity GetById(object id)
        {
            using (var conn = Open())
            {
                TDataModel item = _context.Connection.Find<TDataModel>(id);
                return Mapper.Map<TDataModel, TEntity>(item);
            }
        }

        public IEnumerable<TEntity> GetAll()
        {
            if (_isUpdated)
                return _entities.Value;
            else
            {
                _entities = new Lazy<IList<TEntity>>(() =>
                {
                    using (var conn = Open())
                    {
                        var result = _context.Connection.Table<TDataModel>().ToList();
                        return Mapper.Map<List<TDataModel>, List<TEntity>>(result);
                    }
                });
                return _entities.Value;
            }
        }

        public void DeleteAll()
        {
            using (var conn = Open())
            {
                _context.Connection.DeleteAll<TDataModel>();
                SaveChanges();
            }
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!isDisposing)
            {
                if (isDisposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
                _isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool IsInTransaction 
        {
            get { return _context.Connection.IsInTransaction; }
        }


        public SQLiteConnectionWithCustomLock Open() 
        {
            _context.Connection = new SQLiteConnectionWithCustomLock(_context.Connection.Platform, _context.Connection.DatabasePath);
            _context.Connection.BeginTransaction();
            return _context.Connection;
        }

        public void SaveChanges()
        {
            _context.Connection.Commit();
        }
       
        #endregion

       
    }
}

