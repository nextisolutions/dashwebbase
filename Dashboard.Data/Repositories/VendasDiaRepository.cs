﻿using System;
using Dashboard.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Dashboard.Data
{
	public class VendasDiaRepository : RepositoryBase<VendasDia, VendasDiaDataModel>, IVendasDiaRepository
	{

		#region Constructor

		public VendasDiaRepository (ISQLiteContext context)
			:base(context){}

		#endregion

		#region Methods

		public virtual VendasDia FindByNumber (string name)
		{
			var result = GetAll ();
			return result.SingleOrDefault (vendasDia => vendasDia.loja.Equals (name, StringComparison.OrdinalIgnoreCase));
		}

		#endregion


	}
}

