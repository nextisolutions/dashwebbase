﻿using System;

namespace Dashboard.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IUnitOfWork Open();
        void Commit();
        void Rollback();
        TRepository GetRepository<TRepository>();
    }
}

