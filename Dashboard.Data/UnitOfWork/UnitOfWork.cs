﻿using Dashboard.CrossCutting;
using Dashboard.Domain;
using SQLite.Net;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Dashboard.Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        #region Fields

        private ISQLiteContext _context;

        #endregion

        #region Properties

        public IVendasDiaRepository VendasDia { get; private set; }

        #endregion

        #region Constructor

        public UnitOfWork(ISQLiteContext context)
        {
            _context = context;
            InitializeRepositories(_context);
        }

        #endregion

        #region Methods

        private void InitializeRepositories(ISQLiteContext context)
        {
            VendasDia = Dependency.Container.GetInstance<ISQLiteContext, IVendasDiaRepository>(context);
        }

        #endregion

        #region IUnitOfWork implementation

        public IUnitOfWork Open() 
        {
            if(!_context.Connection.IsInTransaction)
                _context.Connection.BeginTransaction();
            return this;
        }

        public TRepository GetRepository<TRepository>()
        {
            var typeInfo = this.GetType().GetTypeInfo();
            var property = typeInfo
                                .DeclaredProperties
                                .FirstOrDefault(prop => prop.PropertyType == typeof(TRepository));

            if (property != null)
                return (TRepository)property.GetValue(this);

            return default(TRepository);
        }

        public void Commit()
        {
            _context.Connection.Commit();
        }

        public void Rollback()
        {
            _context.Connection.Rollback();
        }

        public void Dispose()
        {

        }

        #endregion

    }
}

