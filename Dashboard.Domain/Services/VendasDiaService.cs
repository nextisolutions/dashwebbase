﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dashboard.Domain
{
    public class VendasDiaService : ServiceBase<VendasDia>, IVendasDiaService
    {
        #region Fields

        private readonly IVendasDiaRepository _repository;

        #endregion

        #region Constructor
        public VendasDiaService(IVendasDiaRepository repository)
            : base(repository)
        {
            _repository = repository;
        }

        #endregion

        #region Methods

        public VendasDia FindByNumber(string name)
        {
            return _repository.FindByNumber(name);
        }

        #endregion

    }
}

