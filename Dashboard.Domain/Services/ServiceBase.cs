﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dashboard.Domain
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity>, IDisposable
        where TEntity : class
    {
        #region Fields

        private readonly IRepositoryBase<TEntity> _repository;

        #endregion

        #region Constructor

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        #endregion

        #region IServiceBase implementation

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        public int Count
        {
            get { return _repository.Count; }
        }

        #endregion
    }
}

