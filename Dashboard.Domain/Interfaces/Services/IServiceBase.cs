﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dashboard.Domain
{
    public interface IServiceBase<TEntity> : IDisposable
        where TEntity : class
    {
        #region Properties

        int Count { get; }

        #endregion

        #region Methods

        IEnumerable<TEntity> GetAll();

        #endregion
    }
}

