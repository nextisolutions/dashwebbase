﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dashboard.Domain
{
	public interface IVendasDiaService: IServiceBase<VendasDia>
	{
		#region Methods

		VendasDia FindByNumber(string name);

		#endregion
	}
}

