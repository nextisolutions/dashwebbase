﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dashboard.Domain
{
	public interface IVendasDiaRepository : IRepositoryBase<VendasDia>
	{
		#region Methods

		VendasDia FindByNumber(string name);

		#endregion
	}
}

