﻿using System;
using SQLite.Net.Interop;
using Xamarin.Forms;
using Dashboard.iOS;
using Dashboard.Data;

[assembly: Dependency(typeof(SQLiteiOSProvider))]
namespace Dashboard.iOS
{
	public class SQLiteiOSProvider : ISQLiteDataPlatform
	{
		#region Fields

		private const string DBName = "DBDashboard.db3";

		#endregion

		#region Constructor

		public SQLiteiOSProvider ()
		{
		}

		#endregion


		#region IProvider implementation

		public string Path {
			get
			{
				var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				return System.IO.Path.Combine(path, DBName);
			}
		}

		public global::SQLite.Net.Interop.ISQLitePlatform Platform
		{
			get { return new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS(); }
		}

		#endregion
	}
}

