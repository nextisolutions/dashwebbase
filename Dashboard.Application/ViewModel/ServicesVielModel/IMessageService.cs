﻿using System;
using System.Threading.Tasks;

namespace Dashboard
{
	public interface IMessageService
	{
		Task ShowAsync(string message);
	}
}



