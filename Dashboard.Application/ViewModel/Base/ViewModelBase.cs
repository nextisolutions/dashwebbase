﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;

namespace Dashboard
{
	public class ViewModelBase<TModel> : IViewModel<TModel>
		where TModel : class, new()
	{

		#region Fields

		private ObservableCollection<TModel> _dataItems; 

		#endregion

		#region Properties

		public ObservableCollection<TModel> DataItems
		{
			get{ return _dataItems; }
			set
			{
				_dataItems = value;
				RaisedPropertyChanged (() => DataItems);
			}
		}

		#endregion

		#region Constructor

		public ViewModelBase ()
		{
			_dataItems = new ObservableCollection<TModel> ();
		}

		#endregion

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		protected void RaisedPropertyChanged<TProperty>(Expression<Func<TProperty>> property)
		{
			var member = property.Body as MemberExpression;
			var pInfo = member.Member as PropertyInfo;

			if (pInfo != null) {
				if (PropertyChanged != null)
					PropertyChanged (this, new PropertyChangedEventArgs (pInfo.Name));
			}
		}

		#endregion
	}
}

