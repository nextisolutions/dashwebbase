﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Dashboard
{
	public interface IViewModel<TModel> : INotifyPropertyChanged
		where TModel : class, new()
	{

		ObservableCollection<TModel> DataItems { get; set; }
	}
}

