﻿using AutoMapper;
using Dashboard.CrossCutting;
using Dashboard.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Windows.Input;

namespace Dashboard
{
	
	public class VendasDiaViewModel  : ViewModelBase<VendasDiaModel> 
	{
		#region Fields

		string _parametroPesquisaData;
		private IVendasDiaAppService _appService;
		private IVendasDiaWService _service;
		private ObservableCollection<VendasDiaModel> _source;
		private ConnectivityInfo _connectivity;
		private DateTime _dataVendaDia = DateTime.Now;
		private bool _isTimerEnabled;
		private int _timeInterval;
		private Command _saveConfigurationCommand; 

		#endregion

		#region Comandos 


		private readonly IMessageService _messageService;

		private bool isLoading;
		public bool IsLoading
		{
			get
			{
				return this.isLoading;
			}

			set
			{
				this.isLoading = value;
				RaisedPropertyChanged (() => IsLoading);
			}
		}

		public Command RefreshCommand
		{
			get
			{
				return new Command (async(c) => {
					await LoadData();
				});
			}
		}




		public Command SaveConfigurationCommand
		{
			get
			{
				return _saveConfigurationCommand ?? (_saveConfigurationCommand = new Command (() => 
				{
					BackgroundTask.Timer (new TimeSpan (TimeInterval), () => 
						{
							if(IsTimerEnabled)
								LoadData();

							return IsTimerEnabled;
						});
				}));
			}
		}

		private bool _isBusy;
		public bool IsBusy
		{
			get { return _isBusy; }
			set
			{
				_isBusy = value;
				RaisedPropertyChanged (() => IsBusy);
			}
		}



		public DateTime DataVendaDia
		{
			get{ return _dataVendaDia; }
			set
			{
				_dataVendaDia = value;
				RaisedPropertyChanged (() => DataVendaDia);
			}
		}

		public bool IsTimerEnabled
		{
			get{ return _isTimerEnabled; }
			set
			{
				_isTimerEnabled = value;
				RaisedPropertyChanged (() => IsTimerEnabled);
			}
		}

		public int TimeInterval
		{
			get{ return _timeInterval; }
			set
			{
				_timeInterval = value;
				RaisedPropertyChanged (() => TimeInterval);
			}
		}

		#endregion 

		#region Constructor

		public VendasDiaViewModel ()
		{
			// Mensagem services
			this._messageService = DependencyService.Get<IMessageService> ();
			//REST SERVICE
			_service = Dependency.Container.GetInstance<IVendasDiaWService>();

			//APPLICATION
			_appService = Dependency.Container.GetInstance<IVendasDiaAppService>();

			//CONNECTIVIY
			_connectivity = new ConnectivityInfo();
			// carega dados
            LoadData();
		}

		#endregion

		#region Methods

		private async Task LoadData() //DateTime? dataVendaDia = null
		{
			// evitar que tenha varios clicks ao botao atualizar
			if ( IsBusy == false) 
			{
	            if (await _connectivity.IsConnected())
           	    {
					try {
                		var dataItems2 = _appService.GetAll();
               			// se tiver conexão
               			var DataMinimaLimite = DateTime.Now.AddDays(-1095);
                		IsBusy = true;
                		var resultItems = await _service.GetVendasDia(DataMinimaLimite > _dataVendaDia ? DateTime.Now : _dataVendaDia);
							
                		// tipo da classe
                		var mapping = Mapper.Map<List<VendasDia>, List<VendasDiaModel>>(resultItems.vendasdia.ToList());
                		_source = new ObservableCollection<VendasDiaModel>(mapping);

						IsBusy = false;  
			
              
                  	    _appService.AddAll(Mapper.Map<List<VendasDiaModel>, List<Dashboard.Domain.VendasDia>>(mapping));
					}
					catch (Exception ex)
					{
						await _messageService.ShowAsync ("Falha ao acessar os dados, tente novamente...");
						IsBusy = false;
						throw;
					}
         	    }
           	    else
           	    { // sem conexão
              	    var dataItems = _appService.GetAll();
               	   _source = new ObservableCollection<VendasDiaModel>(Mapper.Map<List<Dashboard.Domain.VendasDia>, List<VendasDiaModel>>(dataItems.ToList()));
            	}
					
			    DataItems = _source;
	        }
  	    }

		#endregion
	}

}

