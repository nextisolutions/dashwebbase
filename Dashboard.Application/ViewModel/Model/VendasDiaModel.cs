﻿using System;

namespace Dashboard
{
	public class VendasDiaModel
	{
		#region Properties

		public string loja { get; set; }
		public string cidade { get; set; }
		public float total { get; set; }

		#endregion
	}
}

