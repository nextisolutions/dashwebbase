﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Dashboard
{
	public interface IAppServiceBase<TEntity> 
		where TEntity : class
	{
        #region Properties

        int Count { get; }

        #endregion

        #region Methods

        void Add(TEntity entity);
        void AddAll(List<TEntity> entities);
        void Update(TEntity entity);
        void Remove(TEntity entity);
        void DeleteAll();
        TEntity GetById(object id);
        IEnumerable<TEntity> GetAll();

        #endregion
	}
}

