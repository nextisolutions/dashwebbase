﻿using System;
using Dashboard.Domain;
using Dashboard.Data;


namespace Dashboard
{
    public class VendasDiaAppService : AppServiceBase<VendasDia, IVendasDiaRepository>, IVendasDiaAppService
    {
        #region Fields

        private IVendasDiaService _service;

        #endregion

        #region Constructor

        public VendasDiaAppService(IUnitOfWork unitOfWork, IVendasDiaService service)
            : base(unitOfWork)
        {
            _service = service;
        }

        #endregion


    }
}

