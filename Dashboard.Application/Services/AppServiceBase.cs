﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dashboard.Domain;
using Dashboard.Data;

namespace Dashboard
{
    public class AppServiceBase<TEntity, TRepository> : IAppServiceBase<TEntity>, IDisposable
        where TEntity : class
        where TRepository : IRepositoryBase<TEntity>
    {

        #region Fields

        private IUnitOfWork _unitOfWork;
        private IRepositoryBase<TEntity> _repository;

        #endregion

        #region Constructor

        public AppServiceBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = unitOfWork.GetRepository<TRepository>() as IRepositoryBase<TEntity>;
        }

        #endregion

        #region IAppServiceBase implementation

        public int Count
        {
            get { return _repository.Count; }
        }

        public void Add(TEntity entity)
        {
            using (_unitOfWork)
            {
                _repository.Add(entity);
                _unitOfWork.Commit();
            }
        }

        public void AddAll(List<TEntity> entities)
        {
            using (_unitOfWork.Open())
            {	 
                _repository.AddAll(entities);
                _unitOfWork.Commit();
            }
        }

        public void Update(TEntity entity)
        {
            using (_unitOfWork.Open())
            {
                _repository.Update(entity);
                _unitOfWork.Commit();
            }
        }

        public void Remove(TEntity entity)
        {
            using (_unitOfWork.Open())
            {
                _repository.Remove(entity);
                _unitOfWork.Commit();
            }
        }

        public TEntity GetById(object id)
        {
            using (_unitOfWork.Open())
            {
                return _repository.GetById(id);
            }
        }

        public IEnumerable<TEntity> GetAll()
        {
            using (_unitOfWork.Open())
            {
                return _repository.GetAll();
            }
        }

        public void DeleteAll()
        {
            using (_unitOfWork.Open())
            {
                _repository.DeleteAll();
            }
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        #endregion
    }
}

