﻿using System;
using DevExpress;

namespace Dashboard
{
	public class MyLocalizer : DevExpress.Mobile.DataGrid.Localization.GridLocalizer
	{
		public MyLocalizer ()
		{
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.MenuCmd_ColumnGroup, "Agrupar" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.MenuCmd_ColumnSortAscending, "Ordenar: Ascendente" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.MenuCmd_ColumnSortDescending, "Ordenar: Descendente" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.MenuCmd_ColumnSortNone, "Limpar Ordenação" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.MenuCmd_ShowColumnChooser, "Selecionar Colunas" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.ColumnChooserDlg_LabelCaption, "Colunas" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.DialogForm_ButtonCancel, "Cancelar" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.DialogForm_ButtonOk, "OK" );
			AddString(DevExpress.Mobile.DataGrid.Localization.GridStringId.MenuCmd_CloseMenu, "Fechar" );
		}
	}
}

