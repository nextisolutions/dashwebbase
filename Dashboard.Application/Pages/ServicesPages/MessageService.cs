﻿using System;

namespace Dashboard
{
	public class MessageService : IMessageService
	{
		#region IMessageService implementation

		public async System.Threading.Tasks.Task ShowAsync (string message)
		{
			await Dashboard.App.Current.MainPage.DisplayAlert ("Informação", message, "Ok");
		
		}

		#endregion

		public MessageService ()
		{
		}
	}
}





