﻿using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Dashboard
{
	public partial class VendasDiaChartPage
	{
		public VendasDiaChartPage()
		{
			InitializeComponent();

			tooltipBehavior.BackgroundColor = Color.FromHex("#317195");

			Grid.SetRow(Chart, 0);
			Grid.SetRow(label, 1);

			Chart.Series[0].SelectedDataPointIndex = 0;
		}

		private void chart_SelectionChanged(object sender, ChartSelectionEventArgs e)
		{
			var selectedindex = e.SelectedDataPointIndex;
			if (selectedindex < 0)
			{
				label.Text = "Toque na barra para selecionar";
				label.FontSize = 16;
				return;
			}
			label.FontSize = 16;
			var series = e.SelectedSeries;
			if (series == null) return;
			var datapoints = (series.ItemsSource as IList<VendasDiaModel>);

			if (datapoints == null) return;
			var x = datapoints[selectedindex].loja.ToString();
			var y = String.Format("{0:C2}", datapoints[selectedindex].total.ToString());

			label.Text = "Loja : " + x + "  Vendas : R$ " + y;
		}
	}
}
