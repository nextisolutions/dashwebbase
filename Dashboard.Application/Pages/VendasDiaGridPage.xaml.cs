﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Dashboard
{
	public partial class VendasDiaGridPage
	{
		public VendasDiaGridPage()
		{
			InitializeComponent();

			DevExpress.Mobile.DataGrid.Localization.GridLocalizer.Active = new MyLocalizer ();

			Grid.SetRow(xGrid, 0);
		}
	}
}
