﻿using Dashboard.CrossCutting;
using Dashboard.Data;
using Dashboard.Domain;
using Dashboard.Services;
using Xamarin.Forms;

namespace Dashboard
{
	public class IoC
	{
		public static void RegisterDepencies()
		{
            //var container = new LightInject.ServiceContainer ();
            var container = Dependency.Container;

            //REST SERVICES
            container.Register<IVendasDiaWService, VendasDiaWService>();

            //APPLICATION
            container.Register(typeof(IAppServiceBase<>), typeof(AppServiceBase<,>));
            container.Register<IVendasDiaAppService, VendasDiaAppService>();

            //DOMAIN SERVICES
            container.Register(typeof(IServiceBase<>), typeof(ServiceBase<>));
            container.Register<IVendasDiaService, VendasDiaService>();

            //REPOSITORY
            container.Register(typeof(IRepositoryBase<>), typeof(RepositoryBase<,>));
            container.Register<IVendasDiaRepository, VendasDiaRepository>();

            //DATA
            container.Register<ISQLiteDataPlatform>(factory => DependencyService.Get<ISQLiteDataPlatform>(DependencyFetchTarget.GlobalInstance));
            container.Register<ISQLiteContext>(factory => new SQLiteContext(DependencyService.Get<ISQLiteDataPlatform>()));   //container.Register<ISQLiteContext, SQLiteContext> ();
            container.Register(typeof(IUnitOfWork), typeof(UnitOfWork));
		}
	}
}


