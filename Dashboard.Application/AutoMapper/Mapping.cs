﻿using System;
using AutoMapper;

namespace Dashboard
{
	public class Mapping
	{
		public static void Register()
		{
			Mapper.Initialize (c => {

				c.AddProfile<DataModelToDomain>();
				c.AddProfile<DomainToDataModel>();
				c.AddProfile<ViewModelToDomain>();
				c.AddProfile<DomainToViewModel>();
				c.AddProfile<DTOToViewModel>();
			});

		}
	}
}

