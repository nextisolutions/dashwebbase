﻿using System;
using AutoMapper;
using Dashboard.Domain;

namespace Dashboard
{
	public class ViewModelToDomain: Profile
	{
		protected override void Configure ()
		{
			Mapper.CreateMap<VendasDiaModel, VendasDia> ();

		}
	}
}

