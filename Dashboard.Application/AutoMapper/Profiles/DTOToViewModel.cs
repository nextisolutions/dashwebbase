﻿using System;
using AutoMapper;
using Dashboard.Services;

namespace Dashboard
{
	public class DTOToViewModel : Profile
	{
		protected override void Configure ()
		{
			Mapper.CreateMap<VendasDia, VendasDiaModel> ();

		}
	}
}

