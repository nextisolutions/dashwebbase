﻿using System;
using AutoMapper;
using Dashboard.Domain;
using Dashboard.Data;

namespace Dashboard
{
	public class DataModelToDomain : Profile
	{
		protected override void Configure ()
		{
			Mapper.CreateMap<VendasDiaDataModel, VendasDia> ();

		}
	}
}

