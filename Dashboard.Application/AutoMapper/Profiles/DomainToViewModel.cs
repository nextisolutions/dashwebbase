﻿using System;
using AutoMapper;
using Dashboard.Domain;

namespace Dashboard
{
	public class DomainToViewModel: Profile
	{
		protected override void Configure ()
		{
			Mapper.CreateMap<VendasDia, VendasDiaModel> ();

		}
	}
}

