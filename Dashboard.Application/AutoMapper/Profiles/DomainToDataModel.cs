﻿using System;
using AutoMapper;
using Dashboard.Domain;
using Dashboard.Data;

namespace Dashboard
{
	public class DomainToDataModel: Profile
	{
		protected override void Configure ()
		{
			Mapper.CreateMap<VendasDia, VendasDiaDataModel> ();

		}
	}
}

