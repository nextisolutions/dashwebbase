using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using Xamarin.Forms;

namespace Dashboard
{
    public class ControlListViewModel : BindableObject
    {
        public List<MasterSample> MasterSampleLists { get; set; }

        public ControlListViewModel()
        {
            MasterSampleLists = new List<MasterSample>();
            PopulateSamplesList();
        }

        private void PopulateSamplesList()
        {
            var assembly = typeof (App).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("Dashboard.SampleList.SampleList.xml");

            using (var reader = new StreamReader(stream))
            {
                var xmlReader = XmlReader.Create(reader);
                xmlReader.Read();

                while (!xmlReader.EOF)
                {
                    if (xmlReader.Name == "Group" && xmlReader.IsStartElement())
                    {
                        var masterSampleList = new MasterSample
                        {
                            Title = GetDataFromXmlReader(xmlReader, "Title"),
                            ImageID = GetDataFromXmlReader(xmlReader, "ImageId"),
                        };

                        if (Device.OS == TargetPlatform.WinPhone && masterSampleList.Title == "Schedule")
                            continue;

						// Update
						bool isUpdated;
						bool.TryParse(GetDataFromXmlReader(xmlReader, "IsUpdated"), out isUpdated);
						if (isUpdated)
							masterSampleList.Type = "Updated";

						// Preview
						if (!isUpdated) {
							bool isPreview;
							bool.TryParse (GetDataFromXmlReader (xmlReader, "IsPreview"), out isPreview);
							if (isPreview)
								masterSampleList.Type = "Preview";

							// New
							if (!isPreview) {
								bool isNew;
								bool.TryParse (GetDataFromXmlReader (xmlReader, "IsNew"), out isNew);
								if (isNew)
									masterSampleList.Type = "New";
							}
						}

                        MasterSampleLists.Add(masterSampleList);
                    }
                    else if (xmlReader.Name == "Sample" && xmlReader.IsStartElement())
                    {
                        var sampleList = MasterSampleLists[MasterSampleLists.Count - 1];

                        var sampleDetails = new SampleDetails
                        {
                            Title = GetDataFromXmlReader(xmlReader, "Title"),
                            ImageId = GetDataFromXmlReader(xmlReader, "ImageID")
                        };

                        xmlReader.MoveToAttribute("Type");
                        sampleDetails.Type = xmlReader.Value;

                        bool isUpdated;
                        bool.TryParse(GetDataFromXmlReader(xmlReader, "IsUpdated"), out isUpdated);
                        if (isUpdated)
                            sampleDetails.SampleType = "Updated";

                        var isNew = false;
                        if (!isUpdated)
                        {
                            bool.TryParse(GetDataFromXmlReader(xmlReader, "IsNew"), out isNew);
                            if (isNew)
                                sampleDetails.SampleType = "New";
                        }

                        if (sampleList.Type == null && (isUpdated || isNew))
                        {
                            sampleList.Type = "Updated";
                        }

                        sampleList.Samples.Add(sampleDetails);
                    }
                    xmlReader.Read();
                }
            }
        }

        private static string GetDataFromXmlReader(XmlReader reader, string attribute)
        {
            reader.MoveToAttribute(attribute);
            return reader.Value;
        }
    }
}