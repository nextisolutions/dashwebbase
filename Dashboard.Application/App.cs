﻿using System;

using Xamarin.Forms;
using Dashboard.CrossCutting;
using Dashboard.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite.Net;
using Dashboard.Domain;


namespace Dashboard
{
	public class App : Application
	{
		public static double ScreenWidth;

		#region Constructor

		public App ()
		{
			InitConfiguration ();

			if (Device.Idiom != TargetIdiom.Phone && Device.OS == TargetPlatform.Windows)
			{
				MainPage = new ControlsPageWindows();
			}
			else
			{
				NavigationPage page = new NavigationPage(new ControlPage());
//				page.BarBackgroundColor = Color.FromHex("#168DDB");
				page.BarBackgroundColor = Color.FromHex("#317195"); 
				page.BarTextColor = Color.White;

				MainPage = page;
			}
		}

		#endregion

		#region Methods

		private void InitConfiguration()
		{
			DependencyService.Register<IMessageService, MessageService> ();
	        //IoC
			IoC.RegisterDepencies();
			//Mappings.
			Mapping.Register();
			//Database
			CreateDatabase();
		}

		private void CreateDatabase()
		{
			var context = Dependency.Container.GetInstance<ISQLiteContext> ();
            context.CreateTableAsync(new Type[]
            {
                typeof(VendasDiaDataModel)
            });
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

		#endregion
	}
}

