﻿using System;

namespace Dashboard.Services
{
	public class VendasDia
	{
		public string loja { get; set; }
		public string cidade { get; set; }
		public float total { get; set; }

		public class VendasDiaCollection
		{
			public VendasDia[] vendasdia{ get; set; }	
		}
	}
}

