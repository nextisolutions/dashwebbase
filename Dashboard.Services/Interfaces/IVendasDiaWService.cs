﻿using System;
using System.Threading.Tasks;

namespace Dashboard.Services
{
	public interface IVendasDiaWService
	{
		Task<VendasDia.VendasDiaCollection> GetVendasDia (DateTime vendasDiaData);
	}
}

