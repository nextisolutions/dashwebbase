﻿using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;
using ModernHttpClient;

namespace Dashboard.Services
{
	public class VendasDiaWService : IVendasDiaWService
	{

		#region Fields

		private string _vendasDiaData;
		static readonly string ApiUrl = "http://200.213.41.34:8001/vendadia.php?action=get_vendadia&data={0}";  


		#endregion


		#region Constructor

		public VendasDiaWService ()
		{
		}

		#endregion


		#region Methods

		public async Task<VendasDia.VendasDiaCollection> GetVendasDia (DateTime vendasDiaData)
		{
			_vendasDiaData = vendasDiaData.ToString ("yyyy-MM-dd");

			try 
			{ 
				var client = new HttpClient (new NativeMessageHandler ());
				client.Timeout = TimeSpan.FromSeconds(60);

				var response = await client.GetAsync(string.Format(ApiUrl, _vendasDiaData));  

				var vendadiaJson = "{vendasdia:" + response.Content.ReadAsStringAsync ().Result + "}";

				if (vendadiaJson != null) 
				{
				    var items = JsonConvert.DeserializeObject<Dashboard.Services.VendasDia.VendasDiaCollection> (vendadiaJson);
			   	    return items;	
				} else
				{
					return null; 	  
				}	
			} 
			catch (Exception ex) 
			{
				throw;	
			}
		}

		#endregion


	}
}

