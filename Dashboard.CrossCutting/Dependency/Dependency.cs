﻿using System;
using LightInject;

namespace Dashboard.CrossCutting
{
	public class Dependency
	{

		#region Fields

		private static ServiceContainer _container;

		#endregion
		
		#region Properties

		public static ServiceContainer Container
		{
			get{
				if (_container == null)
					_container = new ServiceContainer ();

				return _container;
			}
		}

		#endregion
	}
}

