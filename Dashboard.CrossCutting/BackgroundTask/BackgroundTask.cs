﻿using System;
using Xamarin.Forms;

namespace Dashboard.CrossCutting
{
	public class BackgroundTask
	{
		public static void Timer(TimeSpan interval, Func<bool> callback)
		{
			Device.StartTimer(interval, callback);
		}
	}
}

