﻿using System;
using System.Threading.Tasks;
using Connectivity.Plugin;
using Connectivity.Plugin.Abstractions;

namespace Dashboard.CrossCutting
{
	public class ConnectivityInfo
	{
		#region Constructor

		public ConnectivityInfo()
		{
			// verifica conexão wifi 
			CrossConnectivity.Current.ConnectivityChanged += ConnectivityChanged;
		}

		#endregion

		#region Methods

		public async Task<bool> IsConnected()
		{
			var status = CrossConnectivity.Current.IsConnected; 
	
			try {
				status = await CrossConnectivity.Current.IsRemoteReachable("http://200.213.41.34", 8001, 10000);
			} catch{
				status = false;
			} 

			return status;
		}

		private void ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
		{
			var status = e.IsConnected;
		}

		#endregion
	}
}

